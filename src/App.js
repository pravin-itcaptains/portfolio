import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Overcome Studios</h1>
    </div>
  );
}

export default App;
